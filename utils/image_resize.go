package utils

import (
	"fmt"
	"image"
	"image/jpeg"
	"log"
	"os"
	"path/filepath"

	"github.com/nfnt/resize"
)

func isJpeg(fileName string) bool {

	fileExtension := filepath.Ext(fileName)

	return fileExtension == "jpeg"

	//return false
	//fmt.Println("this is file extension", fileExtension)
}

func GetImageDimension(imagePath string) (int, int) {
	file, err := os.Open(imagePath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
	}

	image, _, err := image.DecodeConfig(file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %v\n", imagePath, err)
	}

	if image.Width >= 128 && image.Height >= 128 {
		ResizeImage(imagePath)
	}

	defer file.Close()
	return image.Width, image.Height
}

func ResizeImage(fileName string) {
	dst := "output"
	filePath := filepath.Join(dst, fileName)
	// open "test.jpg"
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}

	// decode jpeg into image.Image
	is_jpeg := isJpeg(fileName)
	// if is_jpeg nak generate jpeg kat sini tapi png nak generate png,
	if is_jpeg {

	}
	img, err := jpeg.Decode(file)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// resize to width 1000 using Lanczos resampling
	// and preserve aspect ratio
	m := resize.Thumbnail(32, 40, img, resize.Lanczos3)

	//m2 := resize.Thumbnail(64, 80, img, resize.Lanczos3)

	//newFile1 := filepath.Join(dst, fmt.Sprintf("resized_", fileName, ".jpeg"))

	out, err := os.Create("test_resized.jpeg")
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()

	// write new image to file
	jpeg.Encode(out, m, nil)
}

func generateJpeg() {

}

func generatePng() {

}
